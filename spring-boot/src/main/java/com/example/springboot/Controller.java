package com.example.springboot;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @GetMapping("/hello")
    public String hello() {
        return "Hello world!";
    }

    @GetMapping("/error500")
    public ResponseEntity<Object> error500() {
        return ResponseEntity.status(500).build();
    }

    @GetMapping("/slow")
    public String slow() throws InterruptedException {
        Thread.sleep(10000);
        return "done";
    }
}
